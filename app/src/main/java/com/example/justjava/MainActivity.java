package com.example.justjava;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
// the only one global variable in this app
    int quantity = 0;

// this method displays QUANTITY value on the screen
// it takes any number or variable as input parameter and displays it in the defined field
    private void displayQuantity(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }

// this method displays given text in th price_text_view TextView
// it takes any text or variable as input parameter and display it in the linked field
// it is not used anymore, since I've added an intent to send email.
//    private void displayMessage(String message){
//        TextView orderSummaryTextView = (TextView) findViewById(R.id.order_summary_text_view);
//        orderSummaryTextView.setText(message);
//    }

// this method increases quantity by on and show the amount and total price
// it calls the displayQuantity with input parameter of actual number of cups
    public void increment(View view){
        if (quantity == 100){
            Toast toast2 = Toast.makeText(this, R.string.over_100, Toast.LENGTH_LONG);
            toast2.show();
            return;
        }
        quantity = quantity + 1;
        displayQuantity(quantity);
    }

// this method decreases quantity by on and show the amount and total price
// it calls the displayQuantity with input parameter of actual number of cups
    public void decrement(View view){
        if (quantity == 1){
            Toast toast1 = Toast.makeText(this, R.string.less_1, Toast.LENGTH_LONG);
            toast1.show();
            return;}
        quantity = quantity - 1;
        displayQuantity(quantity);
    }

/** calculates the price of the order,
 * @param addWhippedCream comes from submitOrder method
 * @param addChocolate comes from submitOrder method
 * @return total price
 */
    private int calculatePrice(boolean addWhippedCream, boolean addChocolate){
        int basePrice = 50;
        if (addWhippedCream) {
            basePrice += 10;
        }
        if (addChocolate) {
            basePrice += 20;
        }
        int price = basePrice * quantity;
        return price;
    };

//  !!!!!!!!!!! I'm here!!
    /**
     * @param price hz about price
     * @return final message
     * @param addWhippedCream
     * @param addChocolate
     */
    public String createOrderSummary(int price, boolean addWhippedCream, boolean addChocolate, String name){
        String firstLine = getResources().getString(R.string.message_first_line);
        String priceMessage = firstLine + " " + name;
        String secondLine = getResources().getString(R.string.message_second_line);
        priceMessage += secondLine + " " + addWhippedCream;
        String thirdLine = getResources().getString(R.string.message_third_line);
        priceMessage += "\n" + thirdLine + " " + addChocolate;
        String fourthLine = getResources().getString(R.string.message_fourth_line);
        priceMessage += fourthLine + " " + quantity;
        String fifthLine = getResources().getString(R.string.message_fifth_line);
        priceMessage += fifthLine + " " + calculatePrice(addWhippedCream, addChocolate) + " Р";
        String sixthLine = getResources().getString(R.string.message_sixth_line);
        priceMessage += sixthLine;
        return priceMessage;
    }

// this method is called when the BUTTON Order is clicked
    public void submitOrder(View view) {
        CheckBox whippedCreamCheckBox = (CheckBox) findViewById(R.id.whipped_cream_check_box);
        CheckBox chocolateCheckBox = findViewById(R.id.chocolate_check_box);
        EditText nameText = (EditText) findViewById(R.id.edit_text_name);
        boolean hasWhippedCream = whippedCreamCheckBox.isChecked();
        boolean hasChocolate = chocolateCheckBox.isChecked();
        String enteredName = nameText.getText().toString();
        Log.i("MainActivity", "Has whipped cream " + hasWhippedCream);
        Log.i("MainActivity", "Has chocolate " + hasChocolate);
        int price = calculatePrice(hasWhippedCream, hasChocolate);
        String priceMessage = createOrderSummary(price, hasWhippedCream, hasChocolate, enteredName);
        // displayMessage(priceMessage);
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        String subject = getResources().getString(R.string.subject);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject + " " + enteredName);
        intent.putExtra(Intent.EXTRA_TEXT, priceMessage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }

    }

}
